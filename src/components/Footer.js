import React from 'react';
import style from '../mystyle.module.css';


class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <footer>
        <div className={style.copyright}>
          <span>Copyright</span>
        </div>
      </footer>
    )
  }
}

export default Footer;
