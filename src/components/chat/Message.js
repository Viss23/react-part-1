import React from 'react';
import style from '../../mystyle.module.css';
import moment from 'moment';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLiked: false
    }
    this.likeBtn = this.likeBtn.bind(this);
  }

  likeBtn() {
    this.setState({
      isLiked: !this.state.isLiked
    })
  }

  render() {
    const props = this.props;
    const isMyMessage = props.myUserId === props.data.userId;
    const className = isMyMessage ? 'myMessage' : 'message';
    const onlyTime = moment(props.data.createdAt).format('LTS');
    const likeColor = this.state.isLiked ? 'deepskyblue' : 'grey';

    return (
      <div className={style[className]}>
        <div className={style.messageBox}>
          {!isMyMessage && <img className={style.avatar} src={props.data.avatar}></img>}
          <p className={style.messageText}>{props.data.text}</p>
        </div>
        <div className={style.messageFooter}>
          {isMyMessage && <div className={style.delete} onClick={() => this.props.deleteMessage(props.index)}></div>}
          {isMyMessage && <div className={style.edit} onClick={() => this.props.startEditMessage(props.data.text, props.index)}></div>}
          <span>{onlyTime}</span>
          {!isMyMessage && <div style={{ backgroundColor: likeColor }} className={style.like} onClick={this.likeBtn}></div>}
        </div>
      </div>
    )
  }
}

export default Message;