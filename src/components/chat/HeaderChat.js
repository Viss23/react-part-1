import React from 'react';
import style from '../../mystyle.module.css';
import moment from 'moment';

class HeaderChat extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const props = this.props;
    let time = moment(props.lastMessageTime);
    const transformedTime = time.fromNow();

    return (
      <div className={style.headerChat}>
        <div className={style.headerLeft}>
          <span>{props.name}</span>
          <span>{props.participantsAmount} participants</span>
          <span>{props.messagesAmount} messages</span>
        </div>
        <div className={style.headerRight}>
          <span>The last message was {transformedTime}</span>
        </div>
      </div>
    )
  }
}

export default HeaderChat;