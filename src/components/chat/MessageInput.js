import React from 'react';
import style from '../../mystyle.module.css';

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={style.inputWrapper}>
        <span className={style.editingToggle}>Editing:{String(this.props.isEditing)}</span>
        <textarea value={this.props.text} className={style.textAreaSend} onChange={(e) => this.props.onTextChange(e)}></textarea>
        <div className={style.sendBox} onClick={() => {
          if (this.props.isEditing) {
            this.props.editMessage();
          } else {
            this.props.addMessage();
          }
        }}>
          <div className={style.sendSvg}></div>
          <span className={style.sendText}>Send</span>
        </div>
      </div>
    )
  }
}

export default MessageInput;