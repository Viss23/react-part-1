import React from 'react';
import Message from './Message';
import style from '../../mystyle.module.css';
import moment from 'moment';

class MessageList extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {

    const props = this.props;
    const elements = [];
    let previousValue = 0;
    for (let i = 0; i < props.data.length; i++) {
      if (moment(props.data[i].createdAt).fromNow() !== previousValue) {
        elements.push(<span className={style.timeAgo}>{moment(props.data[i].createdAt).fromNow()}</span>)
      }
      previousValue = moment(props.data[i].createdAt).fromNow();
      elements.push(
        <Message
          myUserId={props.myUserId}
          data={props.data[i]} key={i}
          deleteMessage={props.deleteMessage}
          startEditMessage={props.startEditMessage}
          index={i} />
      )
    }

    return (
      <div className={style.messageList}>
        {elements}
      </div>
    )
  }
}

export default MessageList;