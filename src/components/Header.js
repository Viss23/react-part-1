import React from 'react';
import style from '../mystyle.module.css';


class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <header>
        <div className={style.mark}></div>
        <div className={style.logo}></div>
      </header>
    )
  }
}

export default Header;
