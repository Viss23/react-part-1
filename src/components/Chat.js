import React from 'react';
import HeaderChat from './chat/HeaderChat';
import MessageInput from './chat/MessageInput';
import MessageList from './chat/MessageList';
import style from '../mystyle.module.css';
import Loader from './Loader.js';

const URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      myUserName: 'Ruth',
      myUserId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
      myAvatar: 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
      chatName: 'myChat',
      participantsAmount: 23,
      isEditing: false,
      text: '',
      editingIndex: 0
    }
    this.addMessage = this.addMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.startEditMessage = this.startEditMessage.bind(this);
    this.onTextChange = this.onTextChange.bind(this);
    this.editMessage = this.editMessage.bind(this);
  }

  componentDidMount() {
    fetch(URL)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({
          data: data,
          loading: false
        })
      });
  }

  addMessage() {
    const newMessage = {
      avatar: this.state.myAvatar,
      createdAt: new Date().toISOString(),
      editedAt: '',
      id: Math.random().toString(16).slice(2),
      text: this.state.text,
      user: this.state.myUserName,
      userId: this.state.myUserId
    }
    const newData = [...this.state.data, newMessage]
    this.setState({
      ...this.state,
      data: newData,
      text: ''
    })
  }

  deleteMessage(index) {
    const array = [...this.state.data]; //make a copy of the array
    array.splice(index, 1); //delete index
    this.setState({
      data: array
    })
  }

  startEditMessage(text, index) {
    this.setState({
      ...this.state,
      text: text,
      editingIndex: index,
      isEditing: true
    })
  }

  editMessage() {
    const array = [...this.state.data];
    array[this.state.editingIndex].text = this.state.text;
    this.setState({
      data: array,
      text: '',
      isEditing: false
    })
  }

  onTextChange(e) {
    const value = e.target.value;
    this.setState({
      ...this.state,
      text: value
    })
  }

  render() {
    if (this.state.loading) return <Loader />
    const state = this.state;
    const messagesAmount = state.data.length;

    state.data.sort((a, b) => {
      return new Date(b.createdAt) - new Date(a.createdAt)
    })

    const lastMessageTime = state.data[0].createdAt;

    return (

      <div className={style.chat}>
        <HeaderChat
          name={state.chatName}
          participantsAmount={state.participantsAmount}
          messagesAmount={messagesAmount}
          lastMessageTime={lastMessageTime}
        />
        <MessageList
          myUserId={state.myUserId}
          data={state.data}
          startEditMessage={this.startEditMessage}
          deleteMessage={this.deleteMessage}
          editMessage={this.editMessage}
        />
        <MessageInput
          data={state.data}
          editMessage={this.editMessage}
          addMessage={this.addMessage}
          isEditing={this.state.isEditing}
          text={this.state.text}
          onTextChange={this.onTextChange}
        />
      </div>
    )
  }
}

export default Chat;
